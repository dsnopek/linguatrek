module.exports = function (eleventyConfig) {
  eleventyConfig.addLayoutAlias('base-old', 'layouts/base-old.njk');
  eleventyConfig.addLayoutAlias('blog-old', 'layouts/blog-old.njk');
  eleventyConfig.addLayoutAlias('page-old', 'layouts/page-old.njk');
  eleventyConfig.addLayoutAlias('immediate-redirect', 'layouts/immediate-redirect.njk');

  eleventyConfig.addPassthroughCopy('src/*.jpg');
  eleventyConfig.addPassthroughCopy('src/*.png');
  eleventyConfig.addPassthroughCopy('src/*.swf');
  eleventyConfig.addPassthroughCopy('src/*.ico');
  eleventyConfig.addPassthroughCopy('src/*.xml');
  eleventyConfig.addPassthroughCopy('src/*.txt');
  eleventyConfig.addPassthroughCopy('src/files');
  eleventyConfig.addPassthroughCopy('src/download');
  eleventyConfig.addPassthroughCopy('src/misc');
  eleventyConfig.addPassthroughCopy('src/sites/www.linguatrek.com');
  eleventyConfig.addPassthroughCopy('src/sites/default');
  eleventyConfig.addPassthroughCopy('src/sites/all');

  eleventyConfig.addNunjucksFilter('date', require('./filters/nunjucks-dayjs-filter'));
  
  return {
    dir: {
      input: "src",
      includes: "../_includes",
      output: "pages"
    }
  };
};
