#!/usr/bin/env python3

import sys, re
from bs4 import BeautifulSoup

from common import Converter, InvalidInputException

TEMPLATE='''---
permalink: {permalink}
layout: blog-old
title: '{title}'
day: {day}
month: {month}
year: {year}
category: {category}
tags: {tags}
language: {language}
translation: {translation}
---
<div class="content">
{content}
</div>

<div id="comments">
{comments}
</div>
'''

class BlogPostConverter(Converter):
    def convert(self, data, permalink):
        soup = BeautifulSoup(data, 'html.parser')

        params = {
            'permalink': permalink,
            'title': 'Unknown',
            'day': '01',
            'month': 'Jan',
            'year': '1900',
            'category': 'unknown',
            'tags': '[ ]',
            'language': 'xx',
            'translation': '',
            'content': '',
            'comments': '',
        }

        title_element = soup.find('h1', 'node-title')
        if not title_element:
            raise InvalidInputException("Doesn't appear to be a node")

        params['title'] = title_element.string.replace("'", "''")
        params['day'] = soup.find('strong', 'day').string
        params['month'] = soup.find('strong', 'month').string
        params['year'] = soup.find('strong', 'year').string
        params['language'] = soup.find('html')['lang']

        category_link = soup.find('a', href=re.compile(r'^/category/'))
        if category_link:
            params['category'] = category_link.string

        tags = []
        terms_parent = soup.find('div', 'terms')
        if terms_parent:
            for link in terms_parent.find_all('a', href=re.compile('^/tags/')):
                tags.append(link.string)
            if len(tags) > 0:
                params['tags'] = self.dump_yaml_array(tags)

        translation_language = 'pl' if params['language'] == 'en' else 'en'
        translation = soup.find('a', "language-link", lang=translation_language)['href']
        translation = re.sub(r'\?lang=(en|pl)$', '', translation) + '/'
        if translation != permalink:
            params['translation'] = translation

        params['content'] = self.innerHTML(soup.find('div', 'content'))

        comments_parent = soup.find('div', id='comments')
        comments_header = comments_parent.find('h2', text='Post new comment')
        if comments_header:
            comments_header.parent.decompose()
        comments_content = self.innerHTML(comments_parent)
        comments_content = re.sub(r'\?page=(\d+)', r'-page-\1', comments_content)
        params['comments'] = comments_content

        return TEMPLATE.format(**params)

if __name__ == "__main__":
    converter = BlogPostConverter()
    converter.main(sys.argv[0], sys.argv[1:])

