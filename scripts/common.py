
import sys, getopt, os, re
from bs4 import BeautifulSoup

class InvalidInputException(Exception):
    pass

class Converter:
    def convert(self, data, permalink):
        raise NotImplementException()

    def dump_yaml_array(self, tags):
        a = []
        for tag in tags:
            a.append("  - '%s'" % tag.replace("'", "''"))
        return "\n" + "\n".join(a)

    def innerHTML(self, element):
        s = str(element)
        s = re.sub(r'^<' + element.name + r'[^>]*>', '', s, 1)
        s = re.sub(r'</' + element.name + r'>$', '', s, 1)
        return s.strip()

    def filepath_to_permalink(self, input_path):
        parts = input_path.split('/')
        if parts[-1] == 'index.html':
            parts.pop()
        return '/' + '/'.join(parts) + '/'

    def __do_convert(self, data, permalink):
        result = self.convert(data, permalink)
        if type(result) is tuple:
            return result
        return (result, None)

    def convert_recursively(self, input_path, output_path):
        visited = {}

        for root, dirs, files in os.walk(input_path):
            for filename in files:
                if filename != 'index.html':
                    continue

                input_file_path = os.path.join(root, filename)

                root_safe = root
                root_safe = re.sub(r'\?lang=(en|pl)$', '', root_safe)
                root_safe = re.sub(r'\?page=(\d+)$', r'-page-\1', root_safe)
                if root_safe.endswith('?'):
                    root_safe = root_safe[:-1]

                permalink = '/' + root_safe + '/'
                if permalink in visited:
                    continue
                visited[permalink] = True
               
                with open(input_file_path, 'rt') as fd:
                    data = fd.read()
                try:
                    (result, override_output_path) = self.__do_convert(data, permalink)
                except InvalidInputException as e:
                    print ("*** %s: %s" % (input_file_path, e))
                    continue

                if override_output_path:
                    output_file_path = os.path.join(output_path, override_output_path.strip('/')) + '.html'
                else:
                    output_file_path = os.path.join(output_path, root_safe) + '.html'

                output_parent_dir = os.path.dirname(output_file_path)
                if not os.path.exists(output_parent_dir):
                    os.makedirs(output_parent_dir)
 
                with open(output_file_path, 'wt') as fd:
                    fd.write(result)
     
    def main(self, name, input_args):
        try:
            opts, args = getopt.gnu_getopt(input_args, "hro:", ["recursive", "output:"])
        except getopt.GetoptError:
            self.usage()
            sys.exit(2)
        
        input_path = None
        output_path = None
        recursive = False

        for opt, arg in opts:
            if opt == '-h':
                self.usage(name)
                sys.exit()
            elif opt == '-r':
                recursive = True
            elif opt in ('-o', '--output'):
                output_path = arg

        if len(args) != 1:
            print ("Single argument (input file or path) is required\n")
            self.usage(name)
            sys.exit(2)
        input_path = args[0]

        if not os.path.exists(input_path):
            print ("Input path '%s' doesn't exist" % input_path)
            sys.exit(1)
        if os.path.isdir(input_path) != recursive:
            print ("Recursive must be used with an input directory")
            sys.exit(1)
        if recursive and not output_path:
            print ("Recursive must specify an output directory")
            sys.exit(1)

        #print ("input_path: " + input_path)
        #print ("output_path: " + str(output_path))
        #print ("recursive: " + str(recursive))

        if not recursive:
            with open(input_path, 'rt') as fd:
                data = fd.read()

            (result,unused) = self.__do_convert(data, self.filepath_to_permalink(input_path))

            if output_path:
                with open(output_path, 'wt') as fd:
                    fd.write(result)
            else:
                print (result)
        else:
            self.convert_recursively(input_path, output_path)

    def usage(self, name):
        print (name + " <inputfile> -o <outputfile>")
        print (name + " -r <inputdir> -o <outputdir>")



