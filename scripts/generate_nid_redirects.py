#!/usr/bin/env python3

import sys, re
from bs4 import BeautifulSoup

from common import Converter, InvalidInputException

TEMPLATE='''---
permalink: {node_path}
layout: immediate-redirect
title: '{title}'
redirect: {permalink}
---
'''

class NodeRedirectConverter(Converter):
    def convert(self, data, permalink):
        soup = BeautifulSoup(data, 'html.parser')

        params = {
            'permalink': permalink,
            'title': '',
            'nade_path': ''
        }

        title_element = soup.find('h1', 'node-title')
        if not title_element:
            raise InvalidInputException("Doesn't appear to be a node")
        params['title'] = title_element.string.replace("'", "''")

        nid_id_re = re.compile(r'^node-(\d+)$')
        nid_element = soup.find('div', 'node', id=nid_id_re)
        nid_match = nid_id_re.match(nid_element['id'])
        nid = nid_match.group(1)
        params['node_path'] = "/node/%s/" % nid

        return (TEMPLATE.format(**params), "node/%s" % nid)

if __name__ == "__main__":
    converter = NodeRedirectConverter()
    converter.main(sys.argv[0], sys.argv[1:])

