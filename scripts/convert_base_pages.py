#!/usr/bin/env python3

import sys, re
from bs4 import BeautifulSoup

from common import Converter, InvalidInputException

TEMPLATE='''---
permalink: {permalink}
layout: base-old
title: '{title}'
language: {language}
translation: {translation}
---
{content}
'''

class BasePageConverter(Converter):
    def correct_home_pages(self, data):
        data = re.sub(r'/home\?page=', '/home-page-', data)
        data = re.sub(r'/\?page=', '/home-page-', data)
        data = re.sub(r'/-page-', '/home-page-', data)
        data = re.sub(r'\?page=', '-page-', data)
        return data

    def convert(self, data, permalink):
        soup = BeautifulSoup(data, 'html.parser')

        corrected_permalink = self.correct_home_pages(permalink)
        if corrected_permalink == '//':
            corrected_permalink = '/index.html'

        params = {
            'permalink': corrected_permalink,
            'title': 'Unknown',
            'language': 'xx',
            'translation': '',
            'content': '',
        }

        title_element = soup.find('h1')
        if title_element:
            title = title_element.string
        else:
            title = soup.find('title').string
            title = title.replace('Linguatrek | ', '')
            title = title.replace('| Linguatrek', '')

        params['title'] = title.replace("'", "''")
        params['language'] = soup.find('html')['lang']

        translation_language = 'pl' if params['language'] == 'en' else 'en'
        translation = soup.find('a', "language-link", lang=translation_language)['href']
        translation = re.sub(r'\?lang=(en|pl)$', '', translation) + '/'
        if translation != permalink and translation != '//':
            params['translation'] = translation

        params['content'] = self.correct_home_pages(self.innerHTML(soup.find('div', id='main-content')))

        return (TEMPLATE.format(**params), corrected_permalink)

if __name__ == "__main__":
    converter = BasePageConverter()
    converter.main(sys.argv[0], sys.argv[1:])

