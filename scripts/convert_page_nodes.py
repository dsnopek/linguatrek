#!/usr/bin/env python3

import sys, re
from bs4 import BeautifulSoup

from common import Converter, InvalidInputException

TEMPLATE='''---
permalink: {permalink}
layout: page-old
title: '{title}'
language: {language}
body_classes: page-node node-type-page
translation: {translation}
---
<div class="content">
{content}
</div>
'''

class PageNodeConverter(Converter):
    def convert(self, data, permalink):
        soup = BeautifulSoup(data, 'html.parser')

        params = {
            'permalink': permalink,
            'title': 'Unknown',
            'language': 'xx',
            'translation': '',
            'content': '',
        }

        title_element = soup.find('h1', 'node-title')
        if not title_element:
            raise InvalidInputException("Doesn't appear to be a node")

        params['title'] = title_element.string.replace("'", "''")
        params['language'] = soup.find('html')['lang']

        #translation_language = 'pl' if params['language'] == 'en' else 'en'
        #translation = soup.find('a', "language-link", lang=translation_language)['href']
        #translation = re.sub(r'\?lang=(en|pl)$', '', translation) + '/'
        #if translation != permalink:
        #    params['translation'] = translation

        params['content'] = self.innerHTML(soup.find('div', 'content'))

        return TEMPLATE.format(**params)

if __name__ == "__main__":
    converter = PageNodeConverter()
    converter.main(sys.argv[0], sys.argv[1:])

